package com.demo.springbootproject.controller;

import com.demo.springbootproject.model.Department;
import com.demo.springbootproject.repository.DepartmentRepository;
import com.demo.springbootproject.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DepartmentController {
    @Autowired
   private DepartmentService departmentService;
    @Autowired
    private DepartmentRepository departmentRepository;

    @PostMapping("/departments")
     public List<Department> saveDepartment(@RequestBody List<Department> department){
       List<Department> dept= departmentService.saveDepartment((List<Department>) department);
       return dept;
     }

     @GetMapping("/departments")
     public List<Department>fetchDepartmentList(){
        return departmentService.fetchDepartmentList();
     }

     @GetMapping("/departments/{id}")
     public Department fetchDepartmentById(@PathVariable("id") Long departmentId){
              return departmentService.fetchDepartmentById(departmentId);
     }
     @DeleteMapping("/departments/{id}")
     public String deleteDepartmentById(@PathVariable("id") Long departmentId){
        departmentService.deleteDepartmentById(departmentId);
        return"Department deleted successfully";
     }
     @PutMapping("/departments/{id}")
     public Department updateDepartment(@PathVariable("id") Long departmentId,@RequestBody Department department){
        return departmentService.updateDepartment(departmentId,department);
     }


     @GetMapping("/departments/find/{departmentName}")
     public List<Department> getBydepartmentName(@PathVariable String departmentName){
        return departmentService.getByName(departmentName);
     }
}
