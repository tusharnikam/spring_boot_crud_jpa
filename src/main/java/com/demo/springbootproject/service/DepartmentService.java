package com.demo.springbootproject.service;

import com.demo.springbootproject.model.Department;
import org.springframework.stereotype.Service;

import java.util.List;


public interface DepartmentService {

    public List<Department> saveDepartment(List<Department> department);

   public List<Department> fetchDepartmentList();

   public Department fetchDepartmentById(Long departmentId);

    public void deleteDepartmentById(Long departmentId);

   public  Department updateDepartment(Long departmentId, Department department);
    public List<Department> getByName(String departmentName);
}
